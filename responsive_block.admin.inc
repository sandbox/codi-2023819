<?php
/**
 * @file
 * Provides infrequently used functions and hooks for responsive_block.
 */

/**
 * Menu callback: display the responsive block addition form.
 *
 * @see responsive_block_add_block_form_submit()
 */
function responsive_block_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  $form = block_admin_configure($form, $form_state, 'responsive_block', NULL);

  // Other modules should be able to use hook_form_block_add_block_form_alter()
  // to modify this form, so add a base form ID.
  $form_state['build_info']['base_form_id'] = 'block_add_block_form';

  // Prevent block_add_block_form_validate/submit() from being automatically
  // added because of the base form ID by providing these handlers manually.
  $form['#validate'] = array();
  $form['#submit'] = array('responsive_block_add_block_form_submit');

  return $form;
}

/**
 * Menu callback: confirm deletion of menu blocks.
 */
function responsive_block_delete($form, &$form_state, $delta = 0) {
  $config = responsive_block_get_config($delta);
  $title = $config['admin_title'];
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/structure/block', NULL, t('Delete'), t('Cancel'));
}

/**
 * Deletion of menu blocks.
 */
function responsive_block_delete_submit($form, &$form_state) {
  // Remove the menu block configuration variables.
  $delta = $form_state['values']['delta'];
  $block_ids = variable_get('responsive_block_ids', array());
  unset($block_ids[array_search($delta, $block_ids)]);
  sort($block_ids);
  variable_set('responsive_block_ids', $block_ids);
  variable_del("responsive_block_{$delta}_title_link");
  variable_del("responsive_block_{$delta}_admin_title");
  variable_del("responsive_block_{$delta}_parent");
  variable_del("responsive_block_{$delta}_level");
  variable_del("responsive_block_{$delta}_follow");
  variable_del("responsive_block_{$delta}_depth");
  variable_del("responsive_block_{$delta}_expanded");
  variable_del("responsive_block_{$delta}_sort");

  db_delete('block')
    ->condition('module', 'responsive_block')
    ->condition('delta', $delta)
    ->execute();
  db_delete('block_role')
    ->condition('module', 'responsive_block')
    ->condition('delta', $delta)
    ->execute();
  drupal_set_message(t('The block "%name" has been removed.', array('%name' => $form_state['values']['block_title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}


/**
 * Save the new menu block.
 */
function responsive_block_add_block_form_submit($form, &$form_state) {
  // Determine the delta of the new block.
  $block_ids = variable_get('responsive_block_ids', array());
  $delta = empty($block_ids) ? 1 : max($block_ids) + 1;
  $form_state['values']['delta'] = $delta;

  // Save the new array of blocks IDs.
  $block_ids[] = $delta;
  variable_set('responsive_block_ids', $block_ids);

  // Save the block configuration.
  responsive_block_block_save($delta, $form_state['values']);

  // Run the normal new block submission (borrowed from block_add_block_form_submit).
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'region', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $region = !empty($form_state['values']['regions'][$theme->name]) ? $form_state['values']['regions'][$theme->name] : BLOCK_REGION_NONE;
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'status' => 0,
        'status' => (int) ($region != BLOCK_REGION_NONE),
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Menu callback: admin settings form.
 *
 * @return
 *   The settings form used by Responsive block.
 */
function responsive_block_admin_settings_form($form, &$form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Form submission handler.
 */
function responsive_block_admin_settings_form_submit($form, &$form_state) {
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Alters the block admin form to add delete links next to responsive blocks.
 */
function _responsive_block_form_block_admin_display_form_alter(&$form, $form_state) {
  $blocks = module_invoke_all('responsive_block_blocks');
  foreach (variable_get('responsive_block_ids', array()) AS $delta) {
    if (empty($blocks[$delta])) {
      $form['blocks']['responsive_block_' . $delta]['delete'] = array('#type' => 'link', '#title' => t('delete'), '#href' => 'admin/structure/block/delete-responsive-block/' . $delta);
    }
  }

  foreach (array_keys(menu_get_menus(FALSE)) AS $delta) {
    if (empty($form['blocks']['responsive_' . $delta]['region']['#default_value'])) {
      unset($form['blocks']['responsive_' . $delta]);
    }
  }
  foreach (array_keys(menu_list_system_menus()) AS $delta) {
    if (empty($form['blocks']['system_' . $delta]['region']['#default_value'])) {
      unset($form['blocks']['system_' . $delta]);
    }
  }
}

/**
 * Implements hook_block_info().
 */
function _responsive_block_block_info() {
  $blocks = array();
  $deltas = variable_get('responsive_block_ids', array());

  foreach ($deltas AS $delta) {
    $config = responsive_block_get_config($delta);
    $blocks[$delta]['info'] = $config['admin_title'];
    // Responsive blocks can't be cached because each menu item can have
    // a custom access callback. menu.inc manages its own caching.
    $blocks[$delta]['cache'] = DRUPAL_NO_CACHE;
  }
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function _responsive_block_block_configure($delta = '') {
  // Create a pseudo form state.
  $form_state = array('values' => responsive_block_get_config($delta));
  return responsive_block_configure_form(array(), $form_state);
}


/**
 * Returns the configuration form for a responsive block.
 *
 * @param $form_state
 *   array An associated array of configuration options should be present in the
 *   'values' key. If none are given, default configuration is assumed.
 * @return
 *   array The form in Form API format.
 */
function responsive_block_configure_form($form, &$form_state, $breakpoint_group_name = '') {
  $config = array();
  // Get the config from the form state.
  if (!empty($form_state['values'])) {
    $config = $form_state['values'];
  }
  // Merge in the default configuration.
  $config += responsive_block_get_config();

  // Don't display the config form if this delta is exported to code.
  if (!empty($config['exported_to_code'])) {
    $form['exported_message'] = array(
      '#markup' => '<p><em>' . t('Configuration is being provided by code.') . '</em></p>',
    );
    return $form;
  }

  $form['responsive-block-wrapper-start'] = array(
    '#markup' => '<div id="responsive-block-settings">',
    '#weight' => -20,
  );

  // We need a different state if the form is in a Panel overlay.
  if (isset($form['override_title'])) {
    $form['title_link']['#states'] = array(
      'visible' => array(
        ':input[name=override_title]' => array('checked' => FALSE),
      ),
    );
  }

  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => $config['admin_title'],
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this block. If blank, the regular title will be used.'),
  );

  $machine_name = $breakpoint_group_name;
  $mappings = responsive_block_mapping_load('iatse669');
  $mappings = $mappings ? $mappings : new stdClass();

  $form['responsive_block_mapping'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['responsive_block_mapping']['machine_name'] = array(
    '#type' => 'value',
    // '#value' => isset($mappings->machine_name) ? $mappings->machine_name : $machine_name,
    '#value' => 'iatse669',
  );

  $form['responsive_block_mapping']['breakpoint_group'] = array(
    '#type' => 'value',
    // '#value' => isset($mappings->breakpoint_group) ? $mappings->breakpoint_group : $breakpoint_group_name,
    '#value' => 'iatse669',
  );

  if (isset($mappings->id)) {
    $form['responsive_block_mapping']['id'] = array(
      '#type' => 'value',
      '#value' => $mappings->id,
    );
  }

  $breakpoints = array();
  $breakpoint_group = breakpoints_breakpoint_group_load('iatse669');

  $weight = 0;
  foreach ($breakpoint_group->breakpoints as $breakpoint_name) {
    $breakpoint = breakpoints_breakpoint_load_by_fullkey($breakpoint_name);

    if ($breakpoint && $breakpoint->status) {
      $breakpoint->global_weight = $breakpoint->weight;
      $breakpoint->weight = $weight++;
      $breakpoints[$breakpoint_name] = $breakpoint;
    }
  }

  module_load_include('inc', 'block', 'block.admin');
  global $theme_key;
  $blocks = block_admin_display_prepare_blocks($theme_key);

  foreach ($blocks as $block) {
    $block_list[$block['delta']] = $block['info'];
  }

  asort($block_list);

  foreach ($breakpoints as $breakpoint_name => $breakpoint) {
    $label = '1x ' . $breakpoint->name . ' [' . $breakpoint->breakpoint . ']';

    $form['responsive_block_mapping']['mapping'][$breakpoint_name]['1x'] = array(
      '#title' => check_plain($label),
      '#type' => 'select',
      '#options' => $block_list,
      '#default_value' => isset($mappings->mapping[$breakpoint_name]['1x']) ? $mappings->mapping[$breakpoint_name]['1x'] : '',
    );

    // if (isset($breakpoint->multipliers) && !empty($breakpoint->multipliers)) {
    //   foreach ($breakpoint->multipliers as $multiplier => $status) {
    //     if ($status) {
    //       $label = $multiplier . ' ' . $breakpoint->name . ' [' . $breakpoint->breakpoint . ']';
    //       $form['responsive_block_mapping']['mapping'][$breakpoint_name][$multiplier] = array(
    //         '#title' => check_plain($label),
    //         '#type' => 'select',
    //         '#options' => $block_list,
    //         '#default_value' => isset($mappings->mapping[$breakpoint_name][$multiplier]) ? $mappings->mapping[$breakpoint_name][$multiplier] : '',
    //       );
    //     }
    //   }
    // }
  }

  $form['menu-block-wrapper-close'] = array('#markup' => '</div>');

  return $form;
}

/**
 * Implements hook_block_save().
 */
function _responsive_block_block_save($delta = '', $edit = array()) {
  if (!empty($delta)) {
    // Don't save values for an exported block.
    $config = responsive_block_get_config($delta);
    if (empty($config['exported_to_code'])) {
      variable_set("responsive_block_{$delta}_admin_title", $edit['admin_title']);
    }

    $mapping = $edit['responsive_block_mapping'];
    $saved = responsive_block_mapping_save($mapping);
    $group = breakpoints_breakpoint_group_load($mapping['breakpoint_group']);
    if ($saved !== FALSE) {
      drupal_set_message(t('Responsive block mappings for @group were saved.', array('@group' => $group->name)));
    }
    else {
      drupal_set_message(t('Something went wrong while trying to save responsive block mappings for @group', array('@group' => $group->name)), 'error');
    }
  }
}